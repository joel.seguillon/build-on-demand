import React, { Component } from "react";
import { isEmpty } from "lodash";
import pkceChallenge from "pkce-challenge";
import { authToken } from "../utils";
import { userInfo } from "../api";
import "./avatar.css";

export default class Avatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }
  authenticate() {
    sessionStorage.clear();
    const pkceCode = pkceChallenge();
    const state = encodeURIComponent(
      btoa(
        JSON.stringify({
          path: window.location.pathname + window.location.search,
          created_at: Date.now().toString(),
        })
      )
    );
    const redirectUri = encodeURIComponent(
      `${process.env.REACT_APP_PAGES_URL}/auth.html`
    );
    const clientId = process.env.REACT_APP_CLIENT_ID;

    var oauthorizeUrl = `${process.env.REACT_APP_GITLAB_URL}/oauth/authorize?client_id=${clientId}&redirect_uri=${redirectUri}&response_type=code&scope=api&state=${state}`;
    oauthorizeUrl += `&code_challenge=${pkceCode.code_challenge}&code_challenge_method=S256`;

    var tokenUrl = `${
      process.env.REACT_APP_GITLAB_URL
    }/oauth/token?client_id=${clientId}&redirect_uri=${redirectUri}&grant_type=authorization_code&code_verifier=${encodeURIComponent(
      pkceCode.code_verifier
    )}`;

    sessionStorage.setItem("token_url", tokenUrl);
    sessionStorage.setItem("state", state);

    window.location = oauthorizeUrl;
  }
  componentDidMount() {
    var tokenitem = authToken();
    if (tokenitem) {
      userInfo().then((user) => {
        this.setState({ user });
      });
    } else {
      this.authenticate();
    }
  }
  render() {
    let avatar = null;
    if (!isEmpty(this.state.user)) {
      avatar = (
        <img
          className="Avatar"
          src={this.state.user.avatar_url}
          alt={this.state.user.name}
        />
      );
    }
    return avatar;
  }
}
